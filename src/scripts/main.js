import utils from './modules/utils';
import Mask from './modules/mask';

const APP = {
	// сет брейкпоинтов для js
	// должны совпадать с теми что в body:after
	mediaBreakpoint: {
		sm: 576,
		md: 768,
		lg: 1024,
		xl: 1440
	},
	data: {
		openMenu: false,
		fullpageInint: false,
		pageColor: {
			'black': {
				header: 'black',
				sidebar: 'black',
				pageNumber: 'black',
			},
			'orange': {
				header: 'orange',
				sidebar: 'orange',
				pageNumber: '',
			},
			'white-black': {
				header: 'white-black',
				sidebar: 'black',
				pageNumber: '',
			},
			'black-white': {
				header: 'black-white',
				sidebar: '',
				pageNumber: '',
			},
			'orange-white-black': {
				header: 'orange-white-black',
				sidebar: 'black',
				pageNumber: '',
			},
			'black-op': {
				header: 'black-op',
				sidebar: '',
				pageNumber: '',
			},
			'black-op-white-black': {
				header: 'black-op-white-black',
				sidebar: 'black',
				pageNumber: '',
			}
		}
	},
	cache: {},
	initBefore: function() {
		APP.polyfills()
		APP.svgIcons()
		document.documentElement.className =
			document.documentElement.className.replace("no-js", "js");
		APP.pjaxListeners();
	},

	init: function() {
		APP.detectIE();
		APP.lazyload();
		APP.pjaxInit();
		APP.modals();
		APP.initMenu();
		APP.initMap();

		const myMask = new Mask(".js-tel")
		myMask.init()

		APP.buttons();
		APP.closeOnFocusLost();
		APP.limitHeight();
		APP.moveAnimation();
		APP.moveDiagrammAnimation();
		APP.moveNumbersAnimation();

		APP.onePageScroll();
		APP.identifyActiveSection();
		APP.initFeatureSlider();
		APP.initServiceSlider();
		APP.initProjectSlider();
		APP.clientSlider();
		APP.initReviewSlider();
		APP.initNewsSlider();
		APP.initTeamSlider();
		APP.inintTargetSlider();
		APP.initCasesSlider();
		APP.initBlogSlider();
		APP.initArchiveSlider();
		APP.initWebCasesSlider();
		APP.initStyleSlider();
		APP.showJobOpeningNumber();
		APP.initParallax();
		APP.cursor();
	},

	initOnLoad: function() {
		APP.truncateText( document.querySelectorAll('.js-dot') );
	},

	buttons: function() {
		$(document).on('click', '.js-toggle-theme', function(e) {
			let page = document.querySelector('.js-feature-page'),
				header = document.querySelector('.header'),
				sidebar = document.querySelector('.sidebar'),
				pageNumber = document.querySelector('.page-number');

			page.classList.toggle('black');
			header.classList.toggle('black');
			sidebar.classList.toggle('black');
			pageNumber.classList.toggle('black');
			if (this.checked) {
				APP.data.activePage = 'black';
				page.setAttribute('data-color', 'black');
			} else {
				page.removeAttribute('data-color');
				delete APP.data.activePage;
			}
		});

		$(document).on('click', '.js-skills', function(e) {
			e.preventDefault();
			$.fn.fullpage.moveSlideRight();
		});

		$(document).on('click', '.js-menu-back', function(e) {
			e.preventDefault();
			let menu = $(this).parent().parent();
			$(`.top-nav__item[data-submenu="${menu.data('menu')}"]`).removeClass('active');
			menu.removeClass('active');
		});

		$(document).on('click', '.js-toggle-menu', function(e) {
			e.preventDefault();
			let menu = document.querySelector('.js-menu'),
				activeMenuRow = menu.querySelectorAll('li.active'),
				button = e.target,
				subMenus = document.querySelectorAll('.js-sub-menu');
			APP.data.openMenu = APP.data.openMenu ? false : true;

			if (APP.data.hasOwnProperty('activePage')) {
				const pageColor = APP.data.pageColor[APP.data.activePage];
				document.querySelector('.header').classList.toggle(pageColor.header);
			}

			Array.from(activeMenuRow).forEach(function (item) {
				item.classList.remove('active');
			});
			Array.prototype.forEach.call(subMenus, function (item) {
				item.classList.remove('active');
			});

			if (!button.classList.contains('disable')) {
				button.classList.add('disable');
				if (button.classList.contains('active')) {
					button.classList.remove('active');
					menu.classList.remove('active');
					Array.prototype.forEach.call(subMenus, function (item) {
						item.classList.remove('active');
					});
					setTimeout(function () {
						button.classList.remove('disable');
					}, 1000);
				} else {
					button.classList.add('active');
					menu.classList.add('active');
					setTimeout(function () {
						button.classList.remove('disable');
					}, 500);
				}
			}
		});

		$(document).on('click', '.js-map', function(e) {
			e.preventDefault();
			if (e.target.classList.contains('js-map-point')) {
				let currentPoint = e.target,
					activePoint = document.querySelector('.js-map-point.active'),
					city = document.querySelector('.js-map-city');

				activePoint.classList.remove('active');
				currentPoint.classList.add('active');
				city.innerText = currentPoint.innerText;
			}
		});

		$(document).on('click', '.js-search-btn', function(e) {
			e.preventDefault();
			$('.js-search-field').animate({
				width: '165px',
			}).focus();
		});

		$(document).on('click', '.js-footer-open-modal', function(e) {
			e.preventDefault();
			let screenSize = utils.getScreenSize();
			if (screenSize >= APP.mediaBreakpoint.lg && screenSize < APP.mediaBreakpoint.xl) {
				$(e.currentTarget).animate({
					width: '70px',
					right: '-70px',
				});
			} else if (screenSize >= APP.mediaBreakpoint.xl) {
				$(e.currentTarget).animate({
					width: '180px',
					right: '-180px',
				});
			}
			document.querySelector('.js-footer-form').classList.add('show');
		});

		$(document).on('click', '.js-reserch', function(e) {
			e.preventDefault();
			$(this).siblings().removeClass('active').find('p').slideUp();
			$(this).addClass('active').find('p').slideDown();
		});
	},

	// Закрытие меню при клике вне
	closeOnFocusLost: function() {
		document.addEventListener('click', function(e) {
			const trg = e.target;
			if (!trg.closest(".header")) {
				if (APP.data.openMenu) {
					$('.js-toggle-menu').click();
				}
			}
		});
	},

	truncateText: function(selector) {
		const cutText = () => {
			for (let i = 0; i < selector.length; i++) {
				const text = selector[i]
				const elemMaxHeight = parseInt(getComputedStyle(text).maxHeight, 10)
				const elemHeight = text.offsetHeight
				const maxHeight = elemMaxHeight ? elemMaxHeight : elemHeight
				shave(text, maxHeight);
			}
		}

		APP.cache.cutTextListener = utils.throttle(cutText, 100)

		cutText();

		window.addEventListener('resize', APP.cache.cutTextListener);
	},

	lazyload: function() {
		if (typeof APP.myLazyLoad == 'undefined') {
			_regularInit()
		} else {
			_update()
		}

		function _update() {
			// console.log('LazyLoad update');
			APP.myLazyLoad.update();
			utils.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
		}

		function _regularInit() {
			// console.log('LazyLoad first init');
			APP.myLazyLoad = new LazyLoad({
				elements_selector: ".lazyload",
				callback_error: function(el) {
					el.parentElement.classList.add('lazyload-error')
				}
			});
			utils.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
		}
	},

	svgIcons: function () {
		const container = document.querySelector('[data-svg-path]')
		const path = container.getAttribute('data-svg-path')
		const xhr = new XMLHttpRequest()
		xhr.onload = function() {
			container.innerHTML = this.responseText
		}
		xhr.open('get', path, true)
		xhr.send()
	},

	polyfills: function () {
		/**
		 * polyfill for elem.closest
		 */
		(function(ELEMENT) {
			ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
			ELEMENT.closest = ELEMENT.closest || function closest(selector) {
				if (!this) return null;
				if (this.matches(selector)) return this;
				if (!this.parentElement) {return null}
					else return this.parentElement.closest(selector)
				};
		}(Element.prototype));

		/**
		 * polyfill for elem.hasClass
		 */
		Element.prototype.hasClass = function(className) {
			return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
		};
	},

	detectIE: function() {
		/**
		 * detect IE
		 * returns version of IE or false, if browser is not Internet Explorer
		 */

		 (function detectIE() {
		 	var ua = window.navigator.userAgent;

		 	var msie = ua.indexOf('MSIE ');
		 	if (msie > 0) {
				// IE 10 or older => return version number
				var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
				document.querySelector('body').className += ' IE';
			}

			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				var rv = ua.indexOf('rv:');
				var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
				document.querySelector('body').className += ' IE';
			}

			var edge = ua.indexOf('Edge/');
			if (edge > 0) {
				// IE 12 (aka Edge) => return version number
				var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
				document.querySelector('body').className += ' IE';
			}

			// other browser
			return false;
		})();
	},

	modals: function() {
		const closeSpeed = 200;
		const onAjaxLoaded = function () {
			const $modalContent = $(this.content);
			utils.objectFitFallback($('[data-object-fit]', $modalContent));
			utils.textareaAutoResize();

			$('.js-tel', $modalContent).mask('+7 (999) 999-99-99', {
				placeholder: '+7 (___) ___-__-__'
			});

			if ($modalContent.find('.js-file').length) {
				$modalContent.find('.js-file').on('change', function() {
					let preview = $(this).parent().find('.js-preview'),
						curFile = this.files;
					if (curFile.length) {
						preview.text(curFile[0].name);
					}
				})
			}
		}

		$(document).on('click', '.js-ajax-modal', function (e) {
			e.preventDefault();
			const $this = $(this)
			$.magnificPopup.open({
				items: {
					src: $this.attr('href')
				},
				type: 'ajax',
				mainClass: 'mfp-fade mfp-close-style',
				removalDelay: closeSpeed,
				fixedContentPos: true,
				fixedBgPos: true,
				callbacks: {
					open: function () { },
					ajaxContentAdded: onAjaxLoaded,
					close: function () { }
				}
			});
		})

		$(document).on('click', '.js-modal-close', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		})
	},

	initMap: function() {
		let mapWrapper = document.getElementById('map');
		if (mapWrapper) {
			const coordinates = new google.maps.LatLng(47.214800, 39.713553),
				map = new google.maps.Map(mapWrapper, {
					zoom: 17,
					center: coordinates,
					disableDefaultUI: true,
				}),
				marker = new google.maps.Marker({
					position: coordinates,
					map: map,
					icon: '../img/theme/marker.png',
				});;
			google.maps.event.trigger(map, 'resize');
		}
	},

	dataSliders: {},

	initFeatureSlider: function() {
		let slider = $('.js-top-slider'),
			thumbSlider = $('.js-top-slider-thumb');
		if (slider.length && thumbSlider.length) {
			if (utils.getScreenSize() < APP.mediaBreakpoint.lg && !APP.dataSliders.hasOwnProperty('featureSlider')) {
				const topSliderThumb = new Swiper(thumbSlider, {
					slidesPerView: 'auto',
					spaceBetween: 30,
					speed: 800,
					followFinger: true,
					freeMode: false,
					watchSlidesVisibility: true,
					watchSlidesProgress: true,
					on: {
						slideChange: function () {
							topSlider.slideTo(this.activeIndex, 800);
						}
					}
				});
				const topSlider = new Swiper(slider, {
					slidesPerView: 1,
					speed: 800,
					thumbs: {
						swiper: topSliderThumb
					},
					on: {
						slideChange: function () {
							topSliderThumb.slideTo(this.activeIndex, 800);
						}
					}
				});
				APP.dataSliders.featureSlider = [topSliderThumb, topSlider];
			} else if (utils.getScreenSize() >= APP.mediaBreakpoint.lg && APP.dataSliders.hasOwnProperty('featureSlider')) {
				APP.dataSliders.featureSlider.forEach(function(slider) {
					slider.destroy();
				});
				delete APP.dataSliders.featureSlider;
			}
			$('.js-top-thumb').on('mouseenter', function(e) {
				e.preventDefault();
				let activeIndex = $('.js-top-thumb').index(this);
				$('.js-top-thumb').removeClass('active').eq(activeIndex).addClass('active');
				$('.js-top-slide').removeClass('show').eq(activeIndex).addClass('show');
			});
		}
	},

	initServiceSlider: function() {
		let slider = $('.js-services-slider'),
			thumbSlider = $('.js-services-thumb-slider');
		if (slider.length && thumbSlider.length) {
			const servicesSliderThumb = new Swiper(thumbSlider, {
				direction: 'vertical',
				slidesPerView: 'auto',
				freeMode: false,
				followFinger: false,
				centeredSlides: false,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				breakpoints: {
					1023: {
						followFinger: true,
						slidesPerView: 3,
						spaceBetween: 10,
						centeredSlides: true,
					}
				}
			});
			const servicesSlider = new Swiper(slider, {
				slidesPerView: 1,
				speed: 800,
				pagination: {
					el: '.services__counter',
					type: 'custom',
					renderCustom: function (reviewSlider, current, total) {
						return current < 10 ? `0${current}` : current;
					}
				},
				thumbs: {
					swiper: servicesSliderThumb
				}
			});
			servicesSliderThumb.on('slideChange', function () {
				servicesSlider.slideTo(this.activeIndex);
			});
			$('.js-services-thumb').on('mouseenter', function() {
				servicesSlider.slideTo($('.js-services-thumb').index(this));
			})
		}
	},

	initProjectSlider: function() {
		let slider = $('.js-projects-slider'),
			thumbSlider = $('.js-projects-thumb-slider'),
			gallery = $('.js-projects-gallery');

		if (slider.length && thumbSlider.length) {
			const projectSliderThumb = new Swiper(thumbSlider, {
				centeredSlides: true,
				slidesPerView: 3,
				speed: 800,
				freeMode: false,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				breakpoints: {
					1023: {
						slidesPerView: 4,
					}
				}
			});
			const projectSlider = new Swiper(slider, {
				autoplay: {
					delay: 5000,
					disableOnInteraction: false,
				},
				slidesPerView: 1,
				speed: 800,
				navigation: {
					nextEl: '.js-projects-next',
					prevEl: '.js-projects-prev',
				},
				thumbs: {
					swiper: projectSliderThumb
				}
			});
			const gallerySlider = new Swiper(gallery, {
				slidesPerView: 'auto',
				slidesPerGroup: 5,
				allowTouchMove: false,
				breakpoints: {
					1023: {
						allowTouchMove: true,
						centeredSlides: true,
						spaceBetween: 20,
						slidesPerView: 1,
						slidesPerGroup: 1,
						nested: true,
					}
				}
			});

			gallery.on('mouseenter', function() {
				projectSlider.autoplay.stop();
			});

			gallery.on('mouseleave', function () {
				projectSlider.autoplay.start();
			});

			projectSliderThumb.on('slideChange', function () {
				projectSlider.slideTo(this.activeIndex, 800);
			});

			projectSlider.on('slideChange', function () {
				projectSliderThumb.slideTo(this.activeIndex, 800);
			});

			const projectSliderObserver = new IntersectionObserver((entries, projectSliderObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						projectSlider.autoplay.start();
					} else {
						projectSlider.autoplay.stop();
					}
				})
			});
			projectSliderObserver.observe(document.querySelector('.js-projects-slider'));
		}
	},

	clientSlider: function() {
		let slider = $('.js-client-slider');

		if (slider.length) {
			let clientSlider = new Swiper('.js-client-slider', {
				slidesPerView: 10,
				spaceBetween: 40,
				speed: 800,
				freeMode: false,
				navigation: {
					nextEl: '.js-slider-button-next',
					prevEl: '.js-slider-button-prev',
				},
				breakpoints: {
					767: {
						slidesPerView: 3,
					},
					1023: {
						slidesPerView: 5,
					},
					1439: {
						slidesPerView: 7,
					}
				}
			});
			$('.js-slider-button-next').on('mouseenter', function () {
				clientSlider.slideNext();
			});
			$('.js-slider-button-prev').on('mouseenter', function () {
				clientSlider.slidePrev();
			});
		}
	},

	initReviewSlider: function() {
		let slider = $('.js-review-slider');

		if (slider.length) {
			let reviewSlider = new Swiper(slider, {
				slidesPerView: 4,
				slidesPerGroup: 4,
				spaceBetween: 30,
				speed: 800,
				navigation: {
					nextEl: '.js-review-next',
					prevEl: '.js-review-prev',
				},
				pagination: {
					el: '.js-review-number',
					type: 'custom',
					renderCustom: function (reviewSlider, current, total) {
						return current < 10 ? `0${current}` : current;
					}
				},
				breakpoints: {
					767: {
						centeredSlides: true,
						slidesPerView: 1,
						slidesPerGroup: 1,
						spaceBetween: 20,
					},
					1439: {
						centeredSlides: false,
						slidesPerGroup: 3,
						slidesPerView: 3,
						spaceBetween: 30,
					}
				}
			});

			$('.js-review-next').on('mouseenter', function () {
				reviewSlider.slideNext();
			});
			$('.js-review-prev').on('mouseenter', function () {
				reviewSlider.slidePrev();
			});
		}
	},

	initNewsSlider: function() {
		let slider = $('.js-news-slider');

		if (slider.length) {
			let newsSlider = new Swiper(slider, {
				slidesPerView: 'auto',
				slidesPerGroup: 4,
				spaceBetween: 30,
				speed: 800,
				allowTouchMove: false,
				breakpoints: {
					767: {
						centeredSlides: true,
						slidesPerView: 1,
						slidesPerGroup: 1,
						spaceBetween: 20,
						allowTouchMove: true,
					},
					1023: {
						centeredSlides: false,
						slidesPerView: 2,
						slidesPerGroup: 2,
						spaceBetween: 20,
						allowTouchMove: true,
					},
					1439: {
						spaceBetween: 20,
					}
				}
			});
		}
	},

	initTeamSlider: function() {
		let slider = $('.js-team-slider'),
			thumbSlider = $('.js-team-thumb-slider');

		if (slider.length && thumbSlider.length) {
			const teamThumbSLider = new Swiper(thumbSlider, {
				slidesPerView: 3,
				spaceBetween: 26,
				speed: 800,
				centeredSlides: true,
				freeMode: false,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				navigation: {
					nextEl: '.js-team-next',
					prevEl: '.js-team-prev',
				},
				breakpoints: {
					1439: {
						spaceBetween: 30,
					}
				},
			});
			const teamSlider = new Swiper(slider, {
					autoplay: {
						delay: 4000,
						disableOnInteraction: false,
					},
					speed: 800,
					slidesPerView: 1,
					allowTouchMove: false,
					thumbs: {
						swiper: teamThumbSLider
					},
					breakpoints: {
						1023: {
							allowTouchMove: true,
						}
					},
				});

			teamSlider.on('slideChange', function() {
				teamThumbSLider.slideTo(this.activeIndex, 800);
			});

			teamThumbSLider.on('slideChange', function() {
				teamSlider.slideTo(this.activeIndex, 800);
			});

			thumbSlider.on('mouseenter', function() {
				teamSlider.autoplay.stop();
			});

			thumbSlider.on('mouseleave', function() {
				teamSlider.autoplay.start();
			});

			const teamSliderObserver = new IntersectionObserver((entries, teamSliderObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						teamSlider.autoplay.start();
					} else {
						teamSlider.autoplay.stop();
					}
				})
			});
			teamSliderObserver.observe(document.querySelector('.js-team-slider'));
		}
	},

	inintTargetSlider: function() {
		let slider = $('.js-targer-cases');

		if (slider.length) {
			const targetSlider = new Swiper(slider, {
				slidesPerView: 'auto',
				slidesPerGroup: 3,
				spaceBetween: 30,
				speed: 800,
				navigation: {
					nextEl: '.js-target-next',
					prevEl: '.js-target-prev',
				},
				breakpoints: {
					1023: {
						centeredSlides: true,
						slidesPerView: 1,
						slidesPerGroup: 1,
						freeMode: false,
						spaceBetween: 20,
					},
					1439: {
						spaceBetween: 20,
					}
				}
			});
		}
	},

	initCasesSlider: function() {
		let slider = $('.js-cases-slider'),
			thumbSlider = $('.js-cases-thumb-slider'),
			gallery = $('.js-cases-gallery');

		if (slider.length && thumbSlider.length && gallery.length) {
			const casesSliderThumb = new Swiper(thumbSlider, {
				centeredSlides: true,
				slidesPerView: 1,
				speed: 800,
				freeMode: false,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				breakpoints: {
					1023: {
						slidesPerView: 4,
					}
				}
			});
			const casesSlider = new Swiper(slider, {
				autoplay: {
					delay: 5000,
					disableOnInteraction: false,
				},
				navigation: {
					nextEl: '.js-cases-next',
					prevEl: '.js-cases-prev',
				},
				slidesPerView: 1,
				speed: 800,
				thumbs: {
					swiper: casesSliderThumb
				}
			});
			const gallerySlider = new Swiper(gallery, {
				slidesPerView: 'auto',
				slidesPerGroup: 5,
				allowTouchMove: false,
				speed: 800,
				breakpoints: {
					1023: {
						allowTouchMove: true,
						centeredSlides: true,
						spaceBetween: 20,
						slidesPerView: 1,
						slidesPerGroup: 1,
						nested: true,
					}
				}
			});

			gallery.on('mouseenter', function() {
				casesSlider.autoplay.stop();
			});

			gallery.on('mouseleave', function () {
				casesSlider.autoplay.start();
			});

			casesSlider.on('slideChange', function() {
				casesThumbSLider.slideTo(this.activeIndex, 800);
			});

			casesThumbSLider.on('slideChange', function() {
				casesSlider.slideTo(this.activeIndex, 800);
			});

			const casesSliderObserver = new IntersectionObserver((entries, casesSliderObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						casesSlider.autoplay.start();
					} else {
						casesSlider.autoplay.stop();
					}
				})
			});
			casesSliderObserver.observe(document.querySelector('.js-cases-slider'));
		}
	},

	initWebCasesSlider: function() {
		let slider = $('.js-web-cases-slider'),
			gallery = $('.js-web-cases-gallery');

		if (slider.length && gallery.length) {
			const casesSlider = new Swiper(slider, {
				slidesPerView: 1,
				speed: 800,
				navigation: {
					nextEl: '.js-web-cases-next',
					prevEl: '.js-web-cases-prev',
				}
			});
			const gallerySlider = new Swiper(gallery, {
				slidesPerView: 'auto',
				slidesPerGroup: 5,
				allowTouchMove: false,
				breakpoints: {
					1023: {
						allowTouchMove: true,
						centeredSlides: true,
						spaceBetween: 20,
						slidesPerView: 1,
						slidesPerGroup: 1,
						nested: true,
					}
				}
			});
		}
	},

	initBlogSlider: function() {
		let slider = $('.js-blog-slider');
		if (slider.length) {
			const blogSlider = new Swiper(slider, {
				slidesPerView: 'auto',
				slidesPerGroup: 4,
				spaceBetween: 30,
				speed: 800,
				navigation: {
					nextEl: '.js-blog-next',
					prevEl: '.js-blog-prev',
				},
				breakpoints: {
					767: {
						centeredSlides: true,
						slidesPerView: 1,
						slidesPerGroup: 1,
						spaceBetween: 20,
						allowTouchMove: true,
					},
					1023: {
						centeredSlides: false,
						slidesPerView: 2,
						slidesPerGroup: 2,
						spaceBetween: 20,
						allowTouchMove: true,
					},
					1439: {
						spaceBetween: 20,
					}
				}
			});
		}
	},

	initArchiveSlider: function() {
		let slider = $('.js-archive-slider');
		if (slider.length) {
			const archiveSlider = new Swiper(slider, {
				slidesPerView: 4,
				slidesPerGroup: 4,
				slidesPerColumn: 1,
				speed: 800,
				navigation: {
					nextEl: '.js-archive-next',
					prevEl: '.js-archive-prev',
				},
				pagination: {
					el: '.js-review-number',
					type: 'custom',
					renderCustom: function (reviewSlider, current, total) {
						return current < 10 ? `0${current}` : current;
					}
				},
				breakpoints: {
					767: {
						centeredSlides: true,
						slidesPerGroup: 1,
						slidesPerColumn: 3,
						slidesPerView: 1,
					},
					1023: {
						centeredSlides: false,
						slidesPerGroup: 2,
						slidesPerView: 2,
						slidesPerColumn: 2,
					},
				}
			});
		}
	},

	initStyleSlider: function() {
		let slider = $('.js-style-slider'),
			thumbSlider = $('.js-style-thumb-slider'),
			gallery = $('.js-style-gallery');

		if (slider.length && thumbSlider.length && gallery.length) {
			const styleSliderThumb = new Swiper(thumbSlider, {
				centeredSlides: true,
				slidesPerView: 3,
				speed: 800,
				freeMode: false,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				navigation: {
					nextEl: '.js-style-next',
					prevEl: '.js-style-prev',
				},
				breakpoints: {
					1023: {
						slidesPerView: 4,
					}
				}
			});
			const styleSlider = new Swiper(slider, {
				autoplay: {
					delay: 5000,
					disableOnInteraction: false,
				},
				slidesPerView: 1,
				speed: 800,
				thumbs: {
					swiper: styleSliderThumb
				}
			});
			const gallerySlider = new Swiper(gallery, {
				slidesPerView: 'auto',
				slidesPerGroup: 3,
				allowTouchMove: false,
				spaceBetween: 30,
				breakpoints: {
					1023: {
						allowTouchMove: true,
						centeredSlides: true,
						spaceBetween: 20,
						slidesPerView: 1,
						slidesPerGroup: 1,
						nested: true,
					},
					1439: {
						slidesPerGroup: 3,
						spaceBetween: 10,
					}
				}
			});

			styleSliderThumb.on('slideChange', function () {
				styleSlider.slideTo(this.activeIndex, 800);
			});

			styleSlider.on('slideChange', function () {
				styleSliderThumb.slideTo(this.activeIndex, 800);
			});

			const styleSliderObserver = new IntersectionObserver((entries, styleSliderObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						styleSlider.autoplay.start();
					} else {
						styleSlider.autoplay.stop();
					}
				})
			});
			styleSliderObserver.observe(document.querySelector('.js-style-slider'));
		}
	},

	initParallax: function() {
		let scene = document.getElementById('scene');
		if (scene) {
			const parallax = new Parallax(scene, {
				invertX: true,
				invertY: true,
				scalarX: 5,
				scalarY: 5,
			});
		}
	},

	initMenu: function() {
		let subMenus = $('.js-sub-menu');
		$(".js-aim-menu").menuAim({
			rowSelector: 'li',
			tolerance: 50,
			activate: function(li) {
				$(li).addClass('active');
				subMenus.filter(`[data-menu=${$(li).data('submenu')}]`).addClass('active');
			},
			deactivate: function(li){
				let submenu = subMenus.filter(`[data-menu=${$(li).data('submenu')}]`),
					level = submenu.data('level');
				$(li).removeClass('active');
				if (level === 'middle') {
					subMenus.find('li.active').removeClass('active');
					subMenus.filter('[data-level="low"]').removeClass('active');
				}
				submenu.removeClass('active');
			}
		});
		$('.js-nav-link').on('click', function(e) {
			e.preventDefault();
			let link = e.target,
				parent = e.target.parentElement;

			if (!parent.dataset.submenu) {
				location.href = link.href;
			} else {
				if (!link.classList.contains('active')) {
					link.classList.add('active');
				} else {
					location.href = link.href;
				}
			}
		});
	},

	cursor: function() {
		let cursor = document.getElementById("cursor"),
			hoverElements = document.querySelectorAll(".js-cursor-hovered");

		function cursorHover() {
			cursor.classList.add("hover");
		}

		function cursorBlur() {
			cursor.classList.remove("hover");
		}

		function addEvents(elem) {
			elem.addEventListener("mouseover", cursorHover);
			elem.addEventListener("mouseout", cursorBlur);
		}

		if (cursor && hoverElements) {
			document.getElementsByTagName("body")[0].addEventListener("mousemove", function (e) {
				cursor.style.left = e.clientX + "px";
				cursor.style.top = e.clientY + "px";
			});

			cursorBlur();

			Array.prototype.forEach.call(hoverElements, function (item) {
				addEvents(item);
			});
		}
	},

	onePageScroll: function() {
		if ($('#fullpage').length && !APP.data.fullpageInint) {
			APP.initFullpage();
			APP.data.fullpageInint = true;
		} else if ($('#fullpage').length && APP.data.fullpageInint) {
			$.fn.fullpage.destroy('all');
			APP.initFullpage();
		} else if (!$('#fullpage').length && APP.data.fullpageInint) {
			$.fn.fullpage.destroy('all');
			APP.data.fullpageInint = false;
		}
	},

	initFullpage: function() {
		$('#fullpage').fullpage({
			scrollOverflow: true,
			scrollingSpeed: 800,
			lazyLoading: false,
			afterRender: function () {
				let sectionsQuantity = $('.section').length;
				if (sectionsQuantity !== 0) {
					if (sectionsQuantity < 10) {
						sectionsQuantity = `0${sectionsQuantity}`;
					}
					$('.js-page-quantity').text(sectionsQuantity);
					$('.js-page-active').text('01');
				}
			},
			afterLoad: function (origin, destination, direction) {
				let activeSection = $('.section').eq(destination - 1),
					color = activeSection.find('.page').data('color');

				if (color) {
					if (APP.data.hasOwnProperty('activePage')) {
						$('.header').removeClass(APP.data.pageColor[APP.data.activePage].header);
						$('.sidebar').removeClass(APP.data.pageColor[APP.data.activePage].sidebar);
						$('.page-number').removeClass(APP.data.pageColor[APP.data.activePage].pageNumber);
					}
					let newColor = APP.data.pageColor[color];
					APP.data.activePage = color;
					$('.header').addClass(newColor.header);
					$('.sidebar').addClass(newColor.sidebar);
					$('.page-number').addClass(newColor.pageNumber);
				} else {
					if (APP.data.hasOwnProperty('activePage')) {
						$('.header').removeClass(APP.data.pageColor[APP.data.activePage].header);
						$('.sidebar').removeClass(APP.data.pageColor[APP.data.activePage].sidebar);
						$('.page-number').removeClass(APP.data.pageColor[APP.data.activePage].pageNumber);
						delete APP.data.activePage;
					}
				}
			},
			onLeave: function (origin, destination, direction) {
				let nextActiveSection = $('.section').eq(destination - 1);
				if (APP.data.openMenu) {
					$('.js-toggle-menu').click();
				}
				if (nextActiveSection.find('.js-animate-element').length) {
					let animatedElements = nextActiveSection.find('.is-animated');
					nextActiveSection.find('.js-animate-element').removeClass('js-animate-element');
					animatedElements.each(function () {
						let element = $(this),
							animation = element.data('animation');
						element.addClass(animation);
					});
				}
				if (destination < 10) {
					$('.js-page-active').text(`0${destination}`);
				} else {
					$('.js-page-active').text(destination);
				}
			}
		});
	},

	showJobOpeningNumber: function() {
		const sections = Array.from(document.querySelectorAll('.js-job-opening-section'));
		if (sections) {
			const numberObserver = new IntersectionObserver((entries, numberObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						entry.target.classList.add('visible');
					}
				})
			});

			sections.forEach((item) => {
				numberObserver.observe(item);
			});
		}
	},

	moveAnimation: function() {
		const animationElements = Array.from(document.querySelectorAll('.js-animated'));
		if (animationElements.length) {
			const animationElementObserver = new IntersectionObserver((entries, animationElementObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						const animation = entry.target.dataset.animation;
						entry.target.classList.add(animation);
						entry.target.classList.remove('js-animated');
					}
				})
			});

			animationElements.forEach((item) => {
				animationElementObserver.observe(item);
			});
		}
	},
	moveDiagrammAnimation: function() {
		const diagrams = Array.from(document.querySelectorAll('.js-diagram'));
		if (diagrams.length) {
			const diagramObserver = new IntersectionObserver((entries, diagramObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						const progress = entry.target.querySelector('.js-diagram-process'),
							positive = parseInt(entry.target.dataset.positive, 10),
							negative = parseInt(entry.target.dataset.negative, 10);
						progress.style.strokeDasharray = `${Math.round(positive * 100 / (positive + negative))} 100`;
					}
				})
			});

			diagrams.forEach((item) => {
				diagramObserver.observe(item);
			});
		}
	},
	moveNumbersAnimation: function() {
		const numbers = Array.from(document.querySelectorAll('.js-number'));
		if (numbers.length) {
			const numbersObserver = new IntersectionObserver((entries, numbersObserver) => {
				entries.forEach((entry) => {
					if (entry.isIntersecting) {
						$(entry.target).removeClass('js-number').spincrement({
							duration: 1500,
							thousandSeparator: ' '
						});
					}
				})
			});

			numbers.forEach((item) => {
				numbersObserver.observe(item);
			});
		}
	},
	identifyActiveSection: function() {
		let pages = document.querySelectorAll('.page');

		if (pages) {
			let activePage = utils.getActivePage(pages)[0],
				color = activePage.dataset.color;

			if (color) {
				const pageColor = APP.data.pageColor[color];
				$('.header').addClass(pageColor.header);
				$('.sidebar').addClass(pageColor.sidebar);
				APP.data.activePage = color;
			}
		}
	},
	limitHeight: function() {
		let element = document.querySelector('.js-scroll-block');
		if (element) {
			utils.limitHeight(element);
			if (!APP.data.hasOwnProperty('simpleBar')) {
				APP.data.simpleBar = new SimpleBar(element, {
					forceVisible: 'y',
					autoHide: false
				});
			}
		}
	},
	pjaxInit: function () {
		let pjax = new Pjax({
			elements: [".js-pjax"],
			selectors: [".main", "title"],
			cacheBust: false
		})
		document.body.classList.add('pjax-inited', 'pjax-loaded')
	},

	pjaxListeners: function () {
		const app = this;
		document.addEventListener("pjax:send", function () {
			// console.log("Event: pjax:send", arguments)
			document.body.classList.add('pjax-loading')
			document.body.classList.remove('pjax-loaded')
		})

		document.addEventListener("pjax:complete", function () {
			// console.log("Event: pjax:complete", arguments)
			document.body.classList.add('pjax-loading-progress')
		})

		document.addEventListener("pjax:error", function () {
			// console.log("Event: pjax:error", arguments)
		})

		document.addEventListener("pjax:success", function () {
			// console.log("Event: pjax:success", arguments)
			setTimeout(function () {
				document.body.classList.remove('pjax-loading', 'pjax-loading-progress')
				document.body.classList.add('pjax-loaded')
			}, 10);
			APP.init()
			APP.initOnLoad()
		})
	}
};

APP.initBefore();

document.addEventListener('DOMContentLoaded', function() {
	APP.init();
});

window.onload = function() {
	APP.initOnLoad();
};

$(window).resize(function () {
	APP.initFeatureSlider();
	APP.limitHeight();
});
