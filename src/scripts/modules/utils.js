const utils = {
    throttle(callback, limit) {
        var wait = false;
        return function() {
            if (!wait) {
            callback.call();
            wait = true;
            setTimeout(function() {
                wait = false;
            }, limit);
            }
        };
    },
    getScreenSize() {
        let screenSize =
            window
            .getComputedStyle(document.querySelector('body'), ':after')
            .getPropertyValue('content');
        screenSize = parseInt(screenSize.match(/\d+/));
        return screenSize;
    },
    objectFitFallback(elements) {
      // if (true) {
        if ('objectFit' in document.documentElement.style === false) {
            for (let i = 0; i < elements.length; i++) {
            const that = elements[i]
            const imgUrl = that.getAttribute('src') ? that.getAttribute('src') : that.getAttribute('data-src');
            const dataFit = that.getAttribute('data-object-fit')
            let fitStyle
            if (dataFit === 'cover') {
                fitStyle = 'cover'
            } else {
                fitStyle = 'contain'
            }
            const parent = that.parentElement
            if (imgUrl) {
                parent.style.backgroundImage = 'url(' + imgUrl + ')'
                parent.classList.add('fit-img')
                parent.classList.add('fit-img--'+fitStyle)
            }
            }
        }
    },
    textareaAutoResize() {
        Array.prototype.forEach.call(
            document.querySelectorAll('[data-autoresize]'), function (element) {
                element.style.boxSizing = 'border-box';
                var offset = element.offsetHeight - element.clientHeight;
                document.addEventListener('input', function (event) {
                    event.target.style.height = 'auto';
                    event.target.style.height = event.target.scrollHeight + offset + 'px';
                });
                element.removeAttribute('data-autoresize');
            });
    },
    getActivePage(pages) {
        return Array.prototype.filter.call(pages, function(page) {
            let coords = page.getBoundingClientRect();
            return coords.top <= 0 && coords.bottom > 0;
        });
    },
    visible(element) {
        let targetPosition = {
            top: window.pageYOffset + element.getBoundingClientRect().top,
            left: window.pageXOffset + element.getBoundingClientRect().left,
            right: window.pageXOffset + element.getBoundingClientRect().right,
            bottom: window.pageYOffset + element.getBoundingClientRect().bottom
          },
          windowPosition = {
            top: window.pageYOffset,
            left: window.pageXOffset,
            right: window.pageXOffset + document.documentElement.clientWidth,
            bottom: window.pageYOffset + document.documentElement.clientHeight
          };

        return targetPosition.bottom > windowPosition.top &&
          targetPosition.top < windowPosition.bottom &&
          targetPosition.right > windowPosition.left &&
          targetPosition.left < windowPosition.right;
    },
    limitHeight(element) {
        let coords = element.getBoundingClientRect();
        element.style.height = `${window.innerHeight - coords.top}px`;
    }
};

export default utils;
