export default class Mask {
    constructor(selector, regExpString = "+7 (___) ___-__-__") {
        this.elements = document.querySelectorAll(selector);
        this.matrix = regExpString;
    }

    init() {
        this.listeners(this.elements)
    }

    listeners(elements) {
        for (let i = 0; i < elements.length; i++) {
            const input = elements[i];
            input.addEventListener("input", this.mask.bind(this), false);
            input.addEventListener("focus", this.mask.bind(this), false);
            input.addEventListener("blur", this.mask.bind(this), false);
        }
    }

    setCursorPosition(pos, elem) {
        elem.focus();
        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
            const range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select()
        }
    }


    mask(event) {
        let i = 0,
            def = this.matrix.replace(/\D/g, ""),
            input = event.currentTarget,
            val = input.value.replace(/\D/g, "");
        if (def.length >= val.length) val = def;
        input.value = this.matrix.replace(/./g, function(a) {
            return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
        });
        if (event.type == "blur") {
            if (input.value.length == 2) input.value = ""
        } else {
            this.setCursorPosition(input.value.length, input)
        }
    }
}
